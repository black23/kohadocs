Obecná veřejná licence GNU v.3
====================================

Obecná veřejná licence GNU v.3
(GNU GPL v.3)
3. verze, 29. červen 2007

Copyright © 2007 Free Software Foundation, Inc.

Kopírování a distribuce doslovných kopií tohoto licenčního dokumentu jsou povoleny, ale jeho úpravy jsou zakázány. 

**Preambule**

Obecná veřejná licence GNU (v angličtině GNU General Public License, dále jen 
jako “GNU GPL” nebo “GPL”) je svobodná, “copyleft” (část autorských práv 
ponechává a části se zříká) licence pro software a jiná díla.

Většina licencí pro software a jiná díla jsou navrženy tak, aby omezovaly 
svobodu jeho sdílení a úprav. Naproti tomu, Obecná veřejná licence GNU zaručuje 
svobodu sdílení a úprav všech verzí programu – aby byl software svobodný pro 
všechny jeho uživatele. My, ze Free Software Foundation, používáme Obecnou 
veřejnou licenci GNU pro většinu našich programů, ale licence se vztahuje 
i na díla jiných autorů, kteří se je rozhodli zveřejnit právě tímto způsobem. 
Můžete ji rovněž použít pro své programy.

Pokud mluvíme o svobodném softwaru, myslíme tím svobodu používání, nikoliv cenu. 
Naše Obecné veřejné licence jsou navrženy tak, abyste mohli volně šířit kopie 
svobodného softwaru (a pokud chcete, nechat si za to i zaplatit), abyste 
obdrželi zdrojový kód, nebo ho měli možnost získat, abyste mohli tento software 
měnit nebo jeho části použít v nových programech a abyste věděli, že tyto věci 
smíte dělat.

Abychom mohli chránit vaše práva, musíme zabránit tomu, aby vám kdokoliv tato 
práva odepíral, nebo vás žádal, abyste se těchto práv vzdali. Proto i vy máte 
určité povinnosti, které musíte dodržet, pokud šíříte nebo upravujete takový 
software, a to povinnost respektovat svobodu ostatních.

Například, šíříte-li kopie programu, ať již zdarma nebo za poplatek, příjemci 
musíte poskytnout stejná práva, jaké jste sami obdrželi. Musíte zaručit, že 
příjemci rovněž dostanou, anebo mohou získat zdrojový kód. A aby i oni znali 
svá práva, musíte je upozornit na tyto podmínky.

Vývojáři, kteří používají GNU GPL, chrání vaše práva ve dvou krocích: (1) 
zabezpečením autorských práv k softwaru, a (2) nabídkou této Licence, která vám 
dává právoplatné svolení k jeho kopírování, šíření a/nebo jeho úpravě.

Kvůli ochraně každého vývojáře a autora, GPL dává jasně najevo, že pro svobodný 
software neexistuje žádná záruka. V zájmu obou stran, uživatelů i autorů, GPL 
požaduje, aby upravené verze programu byly příslušně označeny, a to kvůli tomu, 
aby za původce případných chyb programu nebyli nesprávně označeni autoři 
původních verzí.

Některá zařízení uživatelům zakazují instalaci nebo spuštění upravených verzí 
softwaru, i když výrobce si takovou možnost ponechal. Toto je z principu 
neslučitelné s cílem ochrany uživatelské svobody – svobody jakkoli software 
měnit. Takové porušování se systematicky vyskytuje tam, kde je program určen 
pro jednotlivce, tedy tam, kde je to nejméně přijatelné. Abychom takovým počinem 
předešli, vytvořili jsme tuto verzi GPL. Jestliže problémy tohoto druhu budou 
nadále vznikat, jsme připraveni je podle potřeby ošetřit v následujících 
verzích GPL.

Závěrem, každý program je neustále ohrožen softwarovými patenty. Státy by neměly 
povolovat patenty pro zamezení vývoje a použití softwaru, který je určen pro 
všeobecné použití. Ale u těch, které tak činí, bychom rádi zamezili nebezpečí, 
že by distributoři svobodného programu obdrželi samostatná patentová osvědčení 
a tím by učinili takový program vázaným. Abychom tomu zamezili, GPL zaručuje, 
že patenty nemohou činit program nesvobodným.

Přesná ustanovení a podmínky pro kopírování, šíření a upravování naleznete níže. 

**USTANOVENÍ a PODMÍNKY**

**0. Definice.**

"Označením “tato Licence” se myslí 3. verze Obecné veřejné licence GNU.

“Autorskými právy” se myslí i zákony příbuzné autorským právům, vztahující se na 
jiné druhy díl, jako např. polovodičové masky.

“Programem” se označuje jakékoli dílo, které může být chráněno autorskými právy 
a je licencováno touto Licencí. Uživatel licence se označuje jako “vy”. 
“Uživatelé licence” a “příjemci licence” mohou být jednotlivci nebo organizace.

“Upravováním” díla se rozumí zkopírování nebo přizpůsobení celého nebo jen části 
díla způsobem, který vyžaduje autorská práva. Nemyslí se tím vytvoření přesné 
kopie. Výsledné dílo se nazývá “upravenou verzí” původního díla nebo dílo 
“založené na” původním díle.

“Chráněným dílem” se myslí neupravená verze Programu nebo dílo založené na 
Programu.

“Šířením” díla se označuje jakákoli činnost, která by vás činila přímo či 
nepřímo odpovědnými za porušení Zákona o ochraně autorských práv, kromě 
spouštění díla na osobním počítači nebo vytváření vlastní kopie. Šíření 
zahrnuje kopírování, distribuci (s nebo bez úpravy), zpřístupnění veřejnosti 
a v některých zemích i další aktivity.

“Zveřejnit” dílo značí jakýkoliv druh šíření, které dalším stranám umožňuje 
výrobu nebo pořízení kopií. Běžné používání díla přes počítačovou síť, bez 
přenosu kopie k uživateli, se nepovažuje za zveřejnění díla.

Interaktivní uživatelské prostředí zobrazuje “Příslušné právní podmínky” v 
takovém rozsahu, že jsou uživateli pohodlně vnímatelné a umístěny na dobře 
viditelném místě a (1) oznam zobrazuje Příslušné právní podmínky a (2) oznamuje 
uživateli, že na dílo se nevztahuje záruka (kromě případů, kdy se záruka 
uděluje), že uživatelé licence mohou dílo na základě této Licence dále 
zveřejňovat a oznamuje, kde si mohou tuto Licenci přečíst. Pokud uživatelské 
prostředí obsahuje seznam uživatelských příkazů, dobře viditelné umístění v 
takovém menu splňuje toto kritérium. 

**1. Zdrojový kód**

“Zdrojový kód” označuje preferovanou formu díla určenou na jeho úpravy. 
“Strojovým kódem” se označuje jakákoli nezdrojová forma díla.

“Standardním rozhraním” se myslí rozhraní, které je buď uznáno jako oficiální 
standard definován uznávanou standardizující autoritou, nebo, v případě rozhraní 
určených pro určitý programovací jazyk, je to rozhraní, které je uznané širokou 
vývojářskou veřejností daného jazyka.

“Systémové knihovny” spustitelného díla zahrnují vše (kromě díla jako celku), 
co (a) je zahrnuto v běžné formě balení hlavního komponentu, ale není součástí 
hlavního komponentu a (b) slouží pouze pro usnadnění práce s hlavní 
komponentou, nebo k implementaci standardního rozhraní tam, kde je implementace 
formou zdrojového kódu veřejnosti povolena. V tomto kontextu se “hlavní 
komponentou” myslí hlavní nezbytná součást (jádro, správce oken, atp.) daného 
operačního systému (pokud nějaký je), na kterém spustitelné dílo běží, nebo 
kompilátor používaný ke spuštění díla, nebo spouštěč strojového kódu používaný 
k běhu díla.

“Úplný zdroj” díla značí veškerý zdrojový kód potřebný pro generování, 
instalaci, spouštění (pro spustitelná díla) strojového kódu a úpravu díla, 
včetně skriptů pro kontrolu těchto úkonů. Nezahrnuje systémové knihovny díla, 
nebo obecné nástroje nebo obecně přístupné svobodné programy, které jsou 
používány (v neupravené podobě) k provedení těchto úkonů a nejsou součástí 
díla. Například, Úplný zdroj zahrnuje soubory pro definici rozhraní přidružené 
ke zdrojovým souborům díla, zdrojový kód sdílených knihoven a dynamicky 
navázané podprogramy, které dílo ke správnému fungování potřebuje – a to buď 
formou datové komunikace, nebo formou kontroly přenosu mezi podprogramy a jinými 
částmi díla.

Úplný zdroj nemusí obsahovat nic, co si mohou uživatelé automaticky vygenerovat 
z jiných částí Úplného zdroje.

Úplný zdroj pro dílo v podobě zdrojového kódu je dané dílo samo. 

**2. Základní privilegia**

Všechna práva zaručená touto Licencí trvají po dobu platnosti autorského práva 
na Program a jsou neodvolatelné, pokud jsou dodrženy podmínky uvedené níže. 
Tato Licence explicitně potvrzuje vaše neomezené právo na spouštění 
neupraveného Programu. Výstup ze spuštění chráněného díla tato Licence upravuje 
pouze v případě, že výstup, vzhledem ke svému obsahu, představuje chráněné dílo 
samo. Tato Licence zároveň uznává vaše právo na “fair use” (správné používání) 
nebo jiný ekvivalent daný zákonem o autorském právu.

Dokud je vaše licence platná, můžete tvořit, spouštět a šířit chráněná díla bez 
jakýchkoli omezení, i když díla sami nezveřejňujete. Pro účely exkluzivních 
úprav děl výhradně ve váš prospěch nebo vytvoření prostředků ke spuštění 
těchto děl, můžete chráněná díla zveřejnit pouze v případě, že v souladu 
s podmínkami této Licence zveřejníte všechny materiály, ke kterým nevlastníte 
autorská práva. A ti, kteří tvoří a spouštějí chráněná díla za vás, musí tak 
činit výhradně vaším jménem, s vaším usměrňováním a pod vaší kontrolou, v 
podmínkách, které jim mimo vztahu s vámi zakazují tvořit jakékoli kopie vašeho 
materiálu, ke kterému jste držitelem autorských práv.

Za jiných okolností zveřejnění díla podléhá podmínkám uvedeným níže. 
Sublicencování není povoleno, odstavec 10 ho činí zbytečným. 

**3. Ochrana zákonných práv uživatelů před Zákonem proti obcházení**

Žádné chráněné dílo by nemělo být považováno za součást efektivních 
technologických prostředků na základě platných právních předpisů podle článku 
11, Smlouvy WIPO o autorských právech, přijaté dne 20. prosince 1996, nebo 
obdobných zákonů zakazujících nebo omezujících obcházení takových prostředků.

Pokud zveřejníte chráněné dílo, zříkáte se jakékoliv právní moci zakázat 
obcházení technologických prostředků kromě případu, kdy by takové obcházení 
bylo provedeno na základě práva vyplývajícího z této Licence. Zároveň se 
zříkáte jakéhokoliv záměru omezit provoz nebo úpravu díla kvůli prosazení 
zákonných práv (v rozporu s uživateli díla) vašich nebo třetí strany na 
zákaz obcházení technologických prostředků. 

**4. Zveřejňování doslovných kopií**

Doslovné kopie zdrojového kódu Programu můžete zveřejňovat tak, jak jste jej 
obdržel, za pomoci libovolného média, ale pouze za předpokladu, že na každé 
kopii uvedete zmínku o autorovi a absenci záruky na vhodném a viditelném místě, 
že všechny zmínky odkazující na tuto Licenci a na jakoukoli přidanou podmínku 
podle odstavce 7 ponecháte beze změn a že zároveň ponecháte nedotčené všechny 
zmínky o absenci záruky a všem příjemcům zprostředkujete kopii této Licence 
spolu s Programem.

Za každou zveřejněnou kopii si můžete účtovat peníze (ale nemusíte) a také 
můžete nabízet služby zákaznické podpory nebo záruku za poplatek. 

**5. Zveřejňování upravených verzí zdroje**

Dílo založené na Programu, nebo úpravy na vytvoření díla založené na Programu, 
můžete zveřejňovat ve formě zdrojového kódu dle platných podmínek odstavce 4, 
za předpokladu, že zároveň splníte všechny následující podmínky: 

a. Dílo musí obsahovat zřetelnou zmínku o tom, že bylo upraveno, spolu s datem 
   jeho úpravy.

b. Dílo musí obsahovat zřetelnou zmínku o tom, že bylo vydáno pod touto Licencí 
   a zmínku o jakýchkoliv přidaných podmínkách v souladu s odstavcem 7. Tento 
   požadavek upravuje požadavek na “ponechání všech zmínek beze změn” 
   z odstavce 4.

c. Celé dílo musíte licencovat touto Licencí pro každého, kdo si kopii opatří, 
   anebo s ní jinak přijde do styku. Licence se, spolu s jakýmikoli dalšími 
   podmínkami podle odstavce 7, proto vztahuje na dílo jako celek a na všechny 
   jeho části, bez ohledu na to, jak je dílo a jeho části balené. Tato Licence 
   nedává povolení k licencování děl jiným způsobem, ale pokud jste toto 
   povolení získali jinak, tato Licence takové povolení neruší.

d. Pokud dílo obsahuje interaktivní uživatelská rozhraní, každé z nich musí 
   zobrazovat Příslušné právní podmínky. Pokud ale Program obsahuje interaktivní 
   uživatelská rozhraní, která nezobrazují Příslušné právní podmínky, vaše dílo 
   nemusí přinutit Program, aby je zobrazoval.

Kompilace chráněného díla s jinými vzájemně nezávislými díly, které ze své 
podstaty nejsou rozšířeními chráněného díla a které s ním nejsou kombinovány 
tak, aby tvořily jeden větší program na paměťovém médiu, či jiném distribučním 
médiu, se nazývá “agregát”, pokud kompilace a její výsledné autorská práva 
nejsou používány k omezování přístupu nebo zákonných práv uživatelů kompilace 
nad míru, kterou samostatná díla povolují. Zahrnutím chráněného díla do agregátu 
tato Licence nenabývá platnosti na jiné části agregátu.

**6. Zveřejňování nezdrojových verzí**

Dílo můžete zveřejňovat prostřednictvím strojového kódu podle odstavců 4 a 5, 
uvedených výše, pokud zveřejníte i strojem-čitelný Úplný zdroj dle ustanovení 
této Licence, jedním z následujících způsobů: 

a. Zveřejníte strojový kód ve fyzickém produktu (včetně fyzického distribučního 
   média), doplněný Úplným zdrojem uloženým na trvalém fyzickém médiu běžně 
   používaném pro výměnu softwaru.

b. Zveřejníte strojový kód ve fyzickém produktu (včetně fyzického distribučního 
   média), doplněný písemnou nabídkou platnou nejméně tři roky a tak dlouho, 
   dokud pro ten daný model produktu nabízíte náhradní díly nebo zákaznickou 
   podporu, abyste komukoliv, kdo si strojový kód opatří, poskytly buď (1) kopii 
   Úplného zdroje pro veškerý software obsažený v produktu a chráněný touto 
   Licencí na trvalém fyzickém médiu běžně používaném pro výměnu softwaru, 
   za cenu nepřesahující náklady vzniklé na fyzické vyhotovení takové kopie, 
   nebo (2) poskytli bezplatný přístup k Úplnému zdroji na síťovém serveru.

c. Zveřejníte jednotlivé kopie strojového kódu s kopií písemné nabídky 
   zpřístupnit Úplný zdroj. Tato alternativa je povolena jen výjimečně, 
   pro nekomerční účely a jen v případě, že jste i vy přijali strojový kód 
   s takovou nabídkou, v souladu s odstavcem 6. b).

d. Zveřejníte strojový kód nabídkou přístupu z určeného místa (zdarma nebo za 
   poplatek) a nabídnete rovnocenný přístup k Úplnému zdroji stejným způsobem, 
   ze stejného místa a bez dodatečných poplatků. Od příjemců musíte vyžadovat, 
   aby Úplný zdroj zkopírovali spolu se strojovým kódem. Pokud je tím místem 
   na zkopírování strojového kódu síťový server, Úplný zdroj může být na jiném 
   serveru (provozovaný vámi nebo třetí stranou), který podporuje ekvivalentní 
   prostředky pro kopírování a za podmínky, že vedle strojového kódu zajistíte 
   jasné instrukce o tom, jak Úplný zdroj najít. Nezávisle na tom, jaký server 
   hostuje Úplný zdroj, jste povinen zajistit, že bude přístupný tak dlouho, 
   dokud bude třeba tyto požadavky naplňovat.

e. Zveřejníte strojový kód pomocí peer-to-peer přenosu, za podmínky, že jiné 
   počítače (peers) informujete, kde je ten strojový kód a Úplný zdroj díla 
   zveřejněn zdarma, v souladu s odstavcem 6. d).

Oddělitelná část strojového kódu, jehož zdrojový kód nespadá do Úplného zdroje 
jako systémová knihovna, nemusí být součástí zveřejnění strojového kódu díla.

“Uživatelský produkt” je buď (1) “zákaznický produkt”, což znamená jakékoliv 
soukromé vlastnictví, které je běžně používané pro soukromé, rodinné účely 
a účely pro domácnost, nebo (2) cokoliv vytvořené a prodávané pro zabudování 
do obytných prostor. Pro určení toho, zda je produkt zákaznickým, sporné 
případy mají být rozhodnuty ve prospěch toho, že jimi jsou. Pro daný produkt 
přijat daným uživatelem, “běžné používání” znamená typické použití daného druhu 
produktu, nezávisle na daném uživateli nebo na konkrétním uživatelově způsobu 
použití, jeho očekávání, nebo očekávání, které se na uživatele kladou. 
Produkt je zákaznickým produktem nezávisle na tom, zda má produkt podstatné 
komerční, průmyslové, nebo ne-zákaznické využití, pokud taková využití 
představují jediný signifikantní způsob použití produktu.

“Instalační informace” pro uživatelský produkt jsou jakékoliv metody, procedury, 
autorizační klíče, nebo jiné informace potřebné k instalaci a spuštění 
upravených verzí chráněného díla v daném uživatelském produktu z upravené verze 
Úplného zdroje. Informace musí zajistit, že se v žádném případě nebude bránit 
nebo zasahovat nepřerušené funkcionalitě upraveného strojového kódu jen proto, 
že byly úpravy provedeny.

Pokud budete strojový kód zveřejňovat podle tohoto odstavce v, nebo se, nebo 
výhradně pro použití v zákaznickém produktu a zveřejnění bude součástí 
transakce, v níž se právo na držení a používání uživatelského produktu převádí 
na příjemce na neomezenou dobu nebo na dobu určitou (bez ohledu na to, jak je 
transakce charakterizována), Úplný zdroj zveřejněn na základě tohoto odstavce 
musí být doplněn o instalační informace. Tento požadavek však neplatí, pokud 
ani vy, ani žádná třetí strana si neponechává možnost instalovat upravený 
strojový kód pro uživatelský produkt (např. dílo bylo instalováno na ROM).

Požadavek poskytnutí instalačních informací nezahrnuje požadavek na pokračování 
v poskytování zákaznické podpory, záruky, nebo aktualizací pro dílo, které bylo 
upraveno nebo instalováno příjemcům, nebo pro uživatelský produkt, ve kterém byl 
upraven nebo instalován. Přístup k síti může být odmítnut, pokud by měla 
samotná úprava věcně a nepříznivě ovlivňovat chod sítě, nebo porušuje pravidla 
a protokoly pro komunikaci na síti.

Zveřejněný Úplný zdroj a uvedené instalační informace musí, v souladu s tímto 
odstavcem, být ve formátu, který je veřejně dokumentovaný (a s veřejně dostupnou 
implementací ve formě zdrojového kódu) a nemůže vyžadovat žádné speciální hesla 
nebo klíče pro rozbalení, čtení a kopírování. 

**7. Dodatečné podmínky**

“Dodatečná práva” jsou podmínky, které doplňují podmínky této Licence výjimkami 
na jednu nebo více podmínek. Dodatečná práva, která se mohou vztahovat na celý 
Program, se považují za součást této Licence, ale jen dokud dodržují platné 
právní předpisy. Pokud se uplatní dodatečná práva pouze na část Programu, daná 
část se má používat zvlášť na základě těchto práv, ale na celý Program se 
vztahují podmínky této Licence bez dodatečných práv.

Pokud zveřejníte kopii chráněného díla, můžete z ní podle vlastní libosti 
odstranit kterékoli další práva, nebo jejich kteroukoliv část. (Dodatečné práva 
mohou být sestaveny tak, aby v některých případech úpravy díla požadovali 
samotné jejich odstranění.) Dodatečné práva můžete přidat na materiál, který 
byl ke chráněnému dílu přidán vámi, nebo pro které vlastníte, nebo můžete vydat, 
příslušná autorská práva.

Bez ohledu na jakákoli jiná ustanovení této Licence můžete pro materiál, který 
byl přidán ke chráněnému dílu (pokud vlastníte autorská práva k materiálu), 
rozšířit podmínky této Licence o podmínky: 

a. Disclaiming warranty or limiting liability differently from the terms
   of sections 15 and 16 of this License; or

b. Requiring preservation of specified reasonable legal notices or
   author attributions in that material or in the Appropriate Legal
   Notices displayed by works containing it; or

c. Prohibiting misrepresentation of the origin of that material, or
   requiring that modified versions of such material be marked in
   reasonable ways as different from the original version; or

d. Limiting the use for publicity purposes of names of licensors or
   authors of the material; or

e. Declining to grant rights under trademark law for use of some trade
   names, trademarks, or service marks; or

f. Requiring indemnification of licensors and authors of that material
   by anyone who conveys the material (or modified versions of it) with
   contractual assumptions of liability to the recipient, for any
   liability that these contractual assumptions directly impose on those
   licensors and authors.

All other non-permissive additional terms are considered "further
restrictions" within the meaning of section 10. If the Program as you
received it, or any part of it, contains a notice stating that it is
governed by this License along with a term that is a further
restriction, you may remove that term. If a license document contains a
further restriction but permits relicensing or conveying under this
License, you may add to a covered work material governed by the terms of
that license document, provided that the further restriction does not
survive such relicensing or conveying.

If you add terms to a covered work in accord with this section, you must
place, in the relevant source files, a statement of the additional terms
that apply to those files, or a notice indicating where to find the
applicable terms.

Additional terms, permissive or non-permissive, may be stated in the
form of a separately written license, or stated as exceptions; the above
requirements apply either way.

**8. Termination.**

You may not propagate or modify a covered work except as expressly
provided under this License. Any attempt otherwise to propagate or
modify it is void, and will automatically terminate your rights under
this License (including any patent licenses granted under the third
paragraph of section 11).

However, if you cease all violation of this License, then your license
from a particular copyright holder is reinstated (a) provisionally,
unless and until the copyright holder explicitly and finally terminates
your license, and (b) permanently, if the copyright holder fails to
notify you of the violation by some reasonable means prior to 60 days
after the cessation.

Moreover, your license from a particular copyright holder is reinstated
permanently if the copyright holder notifies you of the violation by
some reasonable means, this is the first time you have received notice
of violation of this License (for any work) from that copyright holder,
and you cure the violation prior to 30 days after your receipt of the
notice.

Termination of your rights under this section does not terminate the
licenses of parties who have received copies or rights from you under
this License. If your rights have been terminated and not permanently
reinstated, you do not qualify to receive new licenses for the same
material under section 10.

**9. Acceptance Not Required for Having Copies.**

You are not required to accept this License in order to receive or run a
copy of the Program. Ancillary propagation of a covered work occurring
solely as a consequence of using peer-to-peer transmission to receive a
copy likewise does not require acceptance. However, nothing other than
this License grants you permission to propagate or modify any covered
work. These actions infringe copyright if you do not accept this
License. Therefore, by modifying or propagating a covered work, you
indicate your acceptance of this License to do so.

**10. Automatic Licensing of Downstream Recipients.**

Each time you convey a covered work, the recipient automatically
receives a license from the original licensors, to run, modify and
propagate that work, subject to this License. You are not responsible
for enforcing compliance by third parties with this License.

An "entity transaction" is a transaction transferring control of an
organization, or substantially all assets of one, or subdividing an
organization, or merging organizations. If propagation of a covered work
results from an entity transaction, each party to that transaction who
receives a copy of the work also receives whatever licenses to the work
the party's predecessor in interest had or could give under the previous
paragraph, plus a right to possession of the Corresponding Source of the
work from the predecessor in interest, if the predecessor has it or can
get it with reasonable efforts.

You may not impose any further restrictions on the exercise of the
rights granted or affirmed under this License. For example, you may not
impose a license fee, royalty, or other charge for exercise of rights
granted under this License, and you may not initiate litigation
(including a cross-claim or counterclaim in a lawsuit) alleging that any
patent claim is infringed by making, using, selling, offering for sale,
or importing the Program or any portion of it.

**11. Patents.**

A "contributor" is a copyright holder who authorizes use under this
License of the Program or a work on which the Program is based. The work
thus licensed is called the contributor's "contributor version".

A contributor's "essential patent claims" are all patent claims owned or
controlled by the contributor, whether already acquired or hereafter
acquired, that would be infringed by some manner, permitted by this
License, of making, using, or selling its contributor version, but do
not include claims that would be infringed only as a consequence of
further modification of the contributor version. For purposes of this
definition, "control" includes the right to grant patent sublicenses in
a manner consistent with the requirements of this License.

Each contributor grants you a non-exclusive, worldwide, royalty-free
patent license under the contributor's essential patent claims, to make,
use, sell, offer for sale, import and otherwise run, modify and
propagate the contents of its contributor version.

In the following three paragraphs, a "patent license" is any express
agreement or commitment, however denominated, not to enforce a patent
(such as an express permission to practice a patent or covenant not to
sue for patent infringement). To "grant" such a patent license to a
party means to make such an agreement or commitment not to enforce a
patent against the party.

If you convey a covered work, knowingly relying on a patent license, and
the Corresponding Source of the work is not available for anyone to
copy, free of charge and under the terms of this License, through a
publicly available network server or other readily accessible means,
then you must either (1) cause the Corresponding Source to be so
available, or (2) arrange to deprive yourself of the benefit of the
patent license for this particular work, or (3) arrange, in a manner
consistent with the requirements of this License, to extend the patent
license to downstream recipients. "Knowingly relying" means you have
actual knowledge that, but for the patent license, your conveying the
covered work in a country, or your recipient's use of the covered work
in a country, would infringe one or more identifiable patents in that
country that you have reason to believe are valid.

If, pursuant to or in connection with a single transaction or
arrangement, you convey, or propagate by procuring conveyance of, a
covered work, and grant a patent license to some of the parties
receiving the covered work authorizing them to use, propagate, modify or
convey a specific copy of the covered work, then the patent license you
grant is automatically extended to all recipients of the covered work
and works based on it.

A patent license is "discriminatory" if it does not include within the
scope of its coverage, prohibits the exercise of, or is conditioned on
the non-exercise of one or more of the rights that are specifically
granted under this License. You may not convey a covered work if you are
a party to an arrangement with a third party that is in the business of
distributing software, under which you make payment to the third party
based on the extent of your activity of conveying the work, and under
which the third party grants, to any of the parties who would receive
the covered work from you, a discriminatory patent license (a) in
connection with copies of the covered work conveyed by you (or copies
made from those copies), or (b) primarily for and in connection with
specific products or compilations that contain the covered work, unless
you entered into that arrangement, or that patent license was granted,
prior to 28 March 2007.

Nothing in this License shall be construed as excluding or limiting any
implied license or other defenses to infringement that may otherwise be
available to you under applicable patent law.

**12. No Surrender of Others' Freedom.**

If conditions are imposed on you (whether by court order, agreement or
otherwise) that contradict the conditions of this License, they do not
excuse you from the conditions of this License. If you cannot convey a
covered work so as to satisfy simultaneously your obligations under this
License and any other pertinent obligations, then as a consequence you
may not convey it at all. For example, if you agree to terms that
obligate you to collect a royalty for further conveying from those to
whom you convey the Program, the only way you could satisfy both those
terms and this License would be to refrain entirely from conveying the
Program.

**13. Use with the GNU Affero General Public License.**

Notwithstanding any other provision of this License, you have permission
to link or combine any covered work with a work licensed under version 3
of the GNU Affero General Public License into a single combined work,
and to convey the resulting work. The terms of this License will
continue to apply to the part which is the covered work, but the special
requirements of the GNU Affero General Public License, section 13,
concerning interaction through a network will apply to the combination
as such.

**14. Revised Versions of this License.**

The Free Software Foundation may publish revised and/or new versions of
the GNU General Public License from time to time. Such new versions will
be similar in spirit to the present version, but may differ in detail to
address new problems or concerns.

Each version is given a distinguishing version number. If the Program
specifies that a certain numbered version of the GNU General Public
License "or any later version" applies to it, you have the option of
following the terms and conditions either of that numbered version or of
any later version published by the Free Software Foundation. If the
Program does not specify a version number of the GNU General Public
License, you may choose any version ever published by the Free Software
Foundation.

If the Program specifies that a proxy can decide which future versions
of the GNU General Public License can be used, that proxy's public
statement of acceptance of a version permanently authorizes you to
choose that version for the Program.

Later license versions may give you additional or different permissions.
However, no additional obligations are imposed on any author or
copyright holder as a result of your choosing to follow a later version.

**15. Disclaimer of Warranty.**

THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY
APPLICABLE LAW. EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT
HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT
WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE. THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF
THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME
THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.

**16. Limitation of Liability.**

IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING
WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR
CONVEYS THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,
INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT
NOT LIMITED TO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES
SUSTAINED BY YOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE
WITH ANY OTHER PROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN
ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

**17. Interpretation of Sections 15 and 16.**

If the disclaimer of warranty and limitation of liability provided above
cannot be given local legal effect according to their terms, reviewing
courts shall apply local law that most closely approximates an absolute
waiver of all civil liability in connection with the Program, unless a
warranty or assumption of liability accompanies a copy of the Program in
return for a fee.

**END OF TERMS AND CONDITIONS**

**How to Apply These Terms to Your New Programs**

If you develop a new program, and you want it to be of the greatest
possible use to the public, the best way to achieve this is to make it
free software which everyone can redistribute and change under these
terms.

To do so, attach the following notices to the program. It is safest to
attach them to the start of each source file to most effectively state
the exclusion of warranty; and each file should have at least the
"copyright" line and a pointer to where the full notice is found.

::

    one line to give the program's name and a brief idea of what it does.
    Copyright (C) year name of author

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see http://www.gnu.org/licenses/.


Also add information on how to contact you by electronic and paper mail.

If the program does terminal interaction, make it output a short notice
like this when it starts in an interactive mode:

::

    program Copyright (C) year name of author
    This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
    This is free software, and you are welcome to redistribute it
    under certain conditions; type `show c' for details.


The hypothetical commands \`\ ``show w``' and \`\ ``show c``' should
show the appropriate parts of the General Public License. Of course,
your program's commands might be different; for a GUI interface, you
would use an "about box".

You should also get your employer (if you work as a programmer) or
school, if any, to sign a "copyright disclaimer" for the program, if
necessary. For more information on this, and how to apply and follow the
GNU GPL, see http://www.gnu.org/licenses/.

The GNU General Public License does not permit incorporating your
program into proprietary programs. If your program is a subroutine
library, you may consider it more useful to permit linking proprietary
applications with the library. If this is what you want to do, use the
GNU Lesser General Public License instead of this License. But first,
please read http://www.gnu.org/philosophy/why-not-lgpl.html.
