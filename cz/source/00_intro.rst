Představení
============

`Základní informace o systému Koha <#introbasics>`__
------------------------------

Koha je první svobodný integrovaný knihovní systém (ILS) s otevřeným kódem. 
Jeho vývoj sponzorují od roku 1999 knihovny všech různých typů a velikostí. 
Společně s mnoha dobrovolníky a firmami nabízejících placenou podporu. Tyto
subjekty pocházejí z mnoha různých států celého světa.

Víc o systému můžete zjistit na oficiálních stránkách globální komunity Koha:
https://koha-community.org

Informace v českém jazyce můžete získat také na webu české komunity:
https://koha.cz

`Doporučení ohledně systému Koha <#introrecommend>`__
------------------------------------------

 Řadu informací a doporučení (včetně návodů pro vývojáře) můžete získat na 
 oficiálních wikistránkách systému Koha: http://wiki.koha-community.org
Při práci se systémem Koha důrazně doporučujeme používat prohlížeč Firefox. 
Internet Explorer není podporován a pro Google Chrome není systém doposud 
plně optimalizován. 

`Použití tohoto manuálu <#usingmanual>`__
------------------------------------

Tento manuál se neustále vyvíjí a reaguje na změny a nové funkce v systému. Sami můžete posílet
dokumentačnímu týmu Koha posílat návrhy a úpravy jako merge requesty přes Gilab
nebo e-mailovou konferenci 
`koha-docs mailing list 
<https://lists.koha-community.org/cgi-bin/mailman/listinfo/koha-docs>`__.
Manuál je uspořádán podle jednotlivých modulů systému. Na začátku většiny sekcí 
(a také dále v textu) se nachází tip "Naleznete zde". Na těchto řádcích zjistíte, 
jak se k popisované funkci systému dostanete. 

Například: Naleznete zde: Koha > Administrace > Společná nastavení systému 

Tyto instrukce vám ukazují, kde příslušnou položku menu v intranetu 
systému Koha naleznete.
V textu manuálu se vyskytuje množství odkazů na jiné sekce manuálu. Obrázky vám 
pomohou při orientaci v rozhraní. 

`Přispívání do manuálu <#contributing>`__
----------------------------------------------

Tento manuál je neustále ve vývoji, proto je každá aktivita při jeho úpravě vítána. 

`Manuál Koha <http://manual.koha-community.org/>`__ spravuje manager dokumentace.
Neznamená to však, že my ostatní bychom nemohli spolupracovat na zlepšování 
kvality toho manuálu.

Manuál je uložený na Gitu:
http://git.koha-community.org/gitweb/?p=kohadocs.git;a=summary

